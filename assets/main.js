var centerapp = {};
var colapapp = {};

function mainGraph() {

}


function getRand() {
    return Math.round(300 + (Math.random() * 200));
}

function makeRandInt() {
    $('.randnum').each(function(i, e) {
        //console.log(e);
        //getRand("3");
        function makr() {
            //console.log(e);
            //e.html("zzz");
            $(e).html(getRand()); //
            setTimeout(makr, 500);
        }
        makr();
    });
}

function showDDosAlert() {
    let size = getWH();
    $('.alert-overlay').css({
        width: size.ewidth * 3,
        height: '725px'
    });
    let ele = $('.alert-overlay');
    let fadeTO = 500;
    ele.fadeIn(fadeTO, function() {
        ele.fadeIn(fadeTO, function() {
            ele.fadeOut(fadeTO, function() {
                ele.fadeIn(fadeTO, function() {

                });
            })
        });
    })
}

function getWH() {
    //let fwidth = $(window).width();
    let height = $(window).height();
    let width = $(window).width();
    let eheight = height / 4;
    let ewidth = width / 5;
    return {
        eheight: eheight,
        ewidth: ewidth
    }
}

function makeCenter($ele) {
    let size = getWH();
    //let height = 4 * 768;
    let height = $(window).height();
    let width = $(window).width();
    let eheight = size.eheight;
    let ewidth = size.ewidth;
    $($ele).css({
        'height': height + "px",
        'width': width + "px"
    });
    $('.col1,.col3').css({
        'width': (width / 5) + 'px',
        'height': (2 * height / 4) + 'px'
    });
    $('.col4').css({
        'width': ewidth + 'px',
        'height': eheight + 'px'
    });
    $('.col2').css({
        'width': (3 * width / 5) + 'px',
        'height': (2 * height / 4) + 'px'
    });
    $('.row1 .content-row, .col4 .content-row').css({
        margin: (ewidth * 0.05) + 'px',
        height: (eheight - (eheight * 0.1)) + 'px',
    });
}

function installApps() {


    function getRandStatus() {
        return {
            malware: Math.round(Math.random() * 12)
        }
    };

    var appsbn = new Vue({
        el: '#app-sbn',
        data: {
            items: mook.sobn().map(function(item) {
                return {
                    title: item,
                    status: getRandStatus()
                }
            })
        }
    });

    var appsbn = new Vue({
        el: '#app-tinhuy',
        data: {
            items: mook.tinhuy().map(function(item) {
                return {
                    title: item,
                    status: getRandStatus()
                }
            })
        }
    });

    var appsbn = new Vue({
        el: '#app-ubnd',
        data: {
            items: mook.ubnd().map(function(item) {
                return {
                    title: item,
                    status: getRandStatus()
                }
            })
        }
    });

    var appsbn = new Vue({
        el: '#app-cdv',
        data: {
            items: mook.cdv().map(function(item) {
                return {
                    title: item,
                    status: getRandStatus()
                }
            })
        }
    });

    var appsbn = new Vue({
        el: '#app-http',
        data: {
            items: mook.http().map(function(item) {
                return {
                    title: item,
                    status: getRandStatus()
                }
            })
        }
    });


    centerapp = new Vue({
        el: '#appcenter',
        data: {
            mode: 'overview',
            scan: {
                title: "",
                items: []
            }
        }
    });

    colapapp = new Vue({
        el: '#app-colap',
        data: {
            iserror: false
        }
    })
}

app_whitesharkapp = {};

function processColap() {
    colapapp.iserror = true;
    $('.alert-overlay').hide();
    showReport('Phát hiện malware ! Kiểm tra các đơn vị liên đới', 5);
    $('.step1').hide();
    $('.step2').show();

    app_whitesharkapp = new Vue({
        el: '#app_whiteshark',
        data: {
            items: []
        }
    });

    function addSampleData() {
        port = Math.round(Math.random() * 1000);
        ip = Math.round(Math.random() * 255) + '.' + Math.round(Math.random() * 255) + '.' + Math.round(Math.random() * 255);
        type = (Math.random() * 3 > 1) ? 'drop' : 'pass';
        let item = {
            ip: ip,
            port: port,
            type: type
        };
        app_whitesharkapp.items = [item].concat(app_whitesharkapp.items);
        setTimeout(() => {
            addSampleData();
        }, 300);
    };

    addSampleData();

}

function showReport(title, mode) {
    let item = [];
    //alert("T");
    centerapp.scan = {
        title: title,
        items: []
    };
    console.log(mode);
    centerapp.mode = "scanning";
    switch (mode) {
        case 0:
            itemsource = mook.sobn()
            break;
        case 1:
            itemsource = mook.tinhuy()
            break;
        case 2:
            itemsource = mook.ubnd()
            break;
        case 3:
            itemsource = mook.cdv()
            break;
        case 4:
            itemsource = mook.http()
            break;
        case 5:
            itemsource = mook.backlist()
            break;
    }
    console.log(itemsource);
    let i = 0;

    console.log(item);
    centerapp.scan = {
        title: title,
        items: []
        /*itemsource.map(function (e) {
                    return {
                        title: e,
                        malware: getRand(),
                        virus: getRand(),
                        port: getRand()
                    };
                })*/
    }



    let targetCount = itemsource.length;
    arrdisplay = [];

    function pushItem(item) {

        //centerapp.scan.items = arrdisplay.push(item);

        arrdisplay.push({
            title: item,
            malware: 0,
            virus: 0,
            port: 0
        });
        centerapp.scan = {
            title: title,
            items: arrdisplay
        }
        console.log(centerapp.scan.items);
        if (i >= targetCount) {
            setTimeout(() => {
                $('span.scanfake').css({
                    color: '#faf'
                });
                $('span.scanfake').each(function(i, ele) {
                    $(ele).html("Scanning ...");
                    setTimeout(() => {

                        var cnt = $(ele).parent().parent().find('td:first-child').html();
                        if (cnt == "Phòng khoa học công nghệ") {
                            $(ele).css({
                                'color': 'red'
                            });
                            $(ele).html(Math.round(Math.random() * 20) + ' vấn đề');

                        } else {
                            if (Math.round(Math.random() * 5) > 1) {
                                $(ele).css({
                                    'color': '#00FF93'
                                });
                                $(ele).html('An toàn <i class="fa fa-check"></i>');
                            } else {
                                $(ele).css({
                                    'color': 'red'
                                });
                                $(ele).html(Math.round(Math.random() * 20) + ' vấn đề');
                            }
                        }

                        /*
                         if( == "Phòng khoa học công nghệ")

                         if (Math.round(Math.random() * 5) > 1) {
                             $(ele).css({
                                 'color': '#00FF93'
                             });
                             $(ele).html('An toàn <i class="fa fa-check"></i>');
                         } else {
                             $(ele).css({
                                 'color': 'red'
                             });
                             $(ele).html(Math.round(Math.random() * 20) + ' vấn đề');
                         }*/

                    }, Math.random() * 4000);
                });
            }, 100);
            return;
        }
        setTimeout(() => {
            pushItem(itemsource[i]);
            i++;
        }, 100);
    }
    pushItem(itemsource[0]);
    i++;
}

$(function() {
    makeCenter('#app');
    makeRandInt();
    setTimeout(() => {
        //showDDosAlert();
    }, 1000);

    // Get sobn
    installApps();

    $(".nicescroll").niceScroll();
    //mainGraph();


});